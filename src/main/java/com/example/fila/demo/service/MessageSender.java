package com.example.fila.demo.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageSender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void send(String name) {
        System.out.println("Sending message to RabbitMQ...");
        rabbitTemplate.convertAndSend("send-email", String.format("Hello my name is %s", name));
        System.out.println("eejvn");
    }
}
