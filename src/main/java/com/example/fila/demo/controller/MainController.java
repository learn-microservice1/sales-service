package com.example.fila.demo.controller;

import com.example.fila.demo.entity.*;
import com.example.fila.demo.entity.ResponseStatus;
import com.example.fila.demo.repository.SalesDetailRepo;
import com.example.fila.demo.repository.SalesRepo;
import com.example.fila.demo.service.MessageSender;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/sales")
public class MainController {

    @Autowired
    private SalesRepo salesRepo;
    @Autowired
    private SalesDetailRepo salesDetailRepo;

    private final MessageSender messageSender;

    private RestTemplate restTemplate = new RestTemplate();
    private ResponseStatus responseStatus = new ResponseStatus();

    @Autowired
    public MainController(MessageSender messageSender) {
        this.messageSender = messageSender;
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<Sales>> getAllSales(){
        List<Sales> list = salesRepo.findAll();

        if (list == null || list.isEmpty()) {
            return new ResponseEntity<>(list, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/getAllStuff")
    public String getAllStuff(){

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

//        JSONObject request = new JSONObject();
//        request.put("name", req.getCustomer_name());

//        HttpEntity<String> entity = new HttpEntity<String>(JSONObject.valueToString(request), httpHeaders);
        HttpEntity<Object> requestEntity = new HttpEntity<>(httpHeaders);
        HttpEntity entityNew = new HttpEntity(httpHeaders);

//        System.out.println("Req: " + request.toString());
//        System.out.println("Req: " + Utilities.toJson(request));
//        System.out.println("Req: " + JSONObject.valueToString(request));
//        String requestJson = "{ \"meta\": \"NJ\", \"name\": \"cupang\" }";

//        InquiryRq inquiryRq = new InquiryRq();
//        inquiryRq.setName("cupang");
//
//        ObjectMapper objectMapper = new ObjectMapper();
//        String reqString = objectMapper.writeValueAsString(inquiryRq);
//        System.out.println("Req: " + reqString);

//        String response = restTemplate.getForObject("http://localhost:8081/index/getAll", String.class, entityNew);
        String response = restTemplate.getForObject("http://localhost:8081/index/getById/"+1, String.class, entityNew);
        System.out.println("Res: " + response);
        return response;


//        RestTemplate restTemplate = new RestTemplate();
//        List<Stuff> list = (List<Stuff>) restTemplate.getForObject("http://localhost:8081/index/getAll", Stuff.class);
//        return list;
//        List<Stuff> list = salesRepo.findAll();

//        if (stuff == null) {
//            return new ResponseEntity<>(stuff, HttpStatus.NOT_FOUND);
//        }
//        return new ResponseEntity<>(stuff, HttpStatus.OK);
    }

    @GetMapping("/getById/{idItem}")
    public String getById(@PathVariable("idItem") Integer id){

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Object> requestEntity = new HttpEntity<>(httpHeaders);
        HttpEntity entityNew = new HttpEntity(httpHeaders);

        String response = restTemplate.getForObject("http://localhost:8081/index/getById/"+id, String.class, entityNew);
        System.out.println("Res: " + response);
        return response;
    }

    @PostMapping("/inquiry")
    public ResponseStatus inquiry(@RequestBody Inquiry inq){
        Stuff stuff = new Stuff();
//        ResponseEntity<Stuff> stuffResponseEntity = restTemplate.postForEntity("http://localhost:8081/index/byId", stuff, Stuff.class);
        stuff.setId(inq.getStuffId());
        ResponseStatus stuffResponseEntity = restTemplate.postForObject("http://localhost:8081/index/byId", stuff, ResponseStatus.class);
        System.out.println("isi response: "+stuffResponseEntity);
        ObjectMapper mapper = new ObjectMapper();
        Stuff convertValue = mapper.convertValue(stuffResponseEntity.getResponse(), Stuff.class);
//        System.out.println("isi : "+convertValue.getStock());
//        System.out.println("tipe nya: "+stuffResponseEntity.getResponse().getClass().getSimpleName());
//        responseStatus.setCode("200");
//        responseStatus.setResponse(stuffResponseEntity.getResponse());
//        responseStatus.setDescription("Succeed get stuff");
//        responseStatus.setMessage("SUCCESS");

        if (stuffResponseEntity.getCode().equals("202")) {
            if (convertValue.getStock() < inq.getAmount()) {
                responseStatus.setCode("4040");
                responseStatus.setDescription("Out of stock");
                responseStatus.setMessage("FAILED");
//            return responseStatus;
            } else {
                Sales sales = new Sales();
                sales.setCustomerName(inq.getCustomerName());
                sales.setAddress(inq.getAddress());
                sales.setStatus("PENDING");
                sales.setEmail(inq.getEmail());
                sales.setInvoiceNumber("34131");
                sales.setTransactionDate(new Date());
                sales.setQuantity(inq.getAmount());
                salesRepo.save(sales);

//                stuff.setName(convertValue.getName());
//                stuff.setPrice(convertValue.getPrice());
//                stuff.setStock(convertValue.getStock() - inq.getAmount());
//                restTemplate.postForEntity("http://localhost:8081/index/" + inq.getStuffId(), stuff, Stuff.class);
//                ResponseEntity<Stuff> newStuff = restTemplate.postForEntity("http://localhost:8081/index/" + inq.getStuffId(), stuff, Stuff.class);

//                JSONObject combined = new JSONObject();
//                try {
//                    combined.put("Object1", stuffResponseEntity.getResponse());
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                try {
//                    combined.put("Object2", newStuff);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }

                responseStatus.setCode("200");
                responseStatus.setResponse(inq);
                responseStatus.setDescription("Succeed get stuff");
                responseStatus.setMessage("SUCCESS");
            }
        } else {
            responseStatus.setCode("500");
            responseStatus.setResponse(inq);
            responseStatus.setDescription("Failed get stuff data");
            responseStatus.setMessage("FAILED");
        }
        return responseStatus;
    }

    @PostMapping("/payment")
    public ResponseStatus payment(@RequestBody Payment req){
        Sales sales = salesRepo.getSalesByInvoiceNumber(req.getInvoiceNumber());
        System.out.println("isi sales: "+sales);

        Stuff stuff = new Stuff();
        stuff.setId(req.getStuffId());
        ResponseStatus stuffResponseEntity = restTemplate.postForObject("http://localhost:8081/index/byId", stuff, ResponseStatus.class);
        System.out.println("isi stuff: "+stuffResponseEntity);

        if (sales != null || stuffResponseEntity.getCode().equals("202")) {
            // Update status sales
            Sales newSales = new Sales();
            newSales.setId(sales.getId());
            newSales.setCustomerName(req.getCustomerName());
            newSales.setStatus("PAID");
            newSales.setAddress(sales.getAddress());
            newSales.setEmail(sales.getEmail());
            newSales.setInvoiceNumber(req.getInvoiceNumber());
            newSales.setTransactionDate(sales.getTransactionDate());
            newSales.setQuantity(sales.getQuantity());
            Sales resSales = salesRepo.save(newSales);
            System.out.println("isi respon sales: "+resSales);

            ObjectMapper mapper = new ObjectMapper();
            Stuff convertValue = mapper.convertValue(stuffResponseEntity.getResponse(), Stuff.class);
            stuff.setName(convertValue.getName());
            stuff.setPrice(convertValue.getPrice());
            stuff.setStock(convertValue.getStock() - sales.getQuantity());
            ResponseEntity<Stuff> resStuff = restTemplate.postForEntity("http://localhost:8081/index/" + req.getStuffId(), stuff, Stuff.class);
            System.out.println("isi respon stuff: "+resStuff.getStatusCode().toString());

            // save sales detail
            SalesDetail salesDetail = new SalesDetail();
            salesDetail.setmStuffId(req.getStuffId());
            salesDetail.settSalesId(Math.toIntExact(sales.getId()));
            salesDetail.setTotalStuff(sales.getQuantity() * convertValue.getPrice());
            SalesDetail resSalesDetail = salesDetailRepo.save(salesDetail);
            System.out.println("isi respon sales detail: "+resSalesDetail);

            if (resSales == null || !resStuff.getStatusCode().toString().equals("200") || resSalesDetail == null) {
                responseStatus.setCode("500");
                responseStatus.setResponse(req);
                responseStatus.setDescription("Payment failed");
                responseStatus.setMessage("FAILED");
            }

            responseStatus.setCode("200");
            responseStatus.setResponse(req);
            responseStatus.setDescription("Payment succeed");
            responseStatus.setMessage("SUCCESS");
        }
        return responseStatus;
    }

    @PostMapping("/saveSales")
    public ResponseEntity<Sales> saveSales(@RequestBody Sales sales){
        Sales saveSales = salesRepo.save(sales);
        return new ResponseEntity<>(saveSales, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Sales> update(@PathVariable( "id" ) Long id, @RequestBody Sales sales) {
        Sales foo = salesRepo.findById(Math.toIntExact(id))
                .map(salesItem -> {
                    salesItem.setCustomerName(sales.getCustomerName());
                    salesItem.setAddress(sales.getAddress());
                    salesItem.setStatus(sales.getStatus());
                    salesItem.setEmail(sales.getEmail());
                    salesItem.setInvoiceNumber(sales.getInvoiceNumber());
                    salesItem.setTransactionDate(sales.getTransactionDate());
                    return salesRepo.save(salesItem);
                })
                .orElseGet(() -> {
                    sales.setId(id);
                    return salesRepo.save(sales);
                });

        return new ResponseEntity<>(foo, HttpStatus.OK);
    }

    @PostMapping(value = "/send-email")
    public Map<String, String> hello(@RequestBody Map<String, String> request) {
        messageSender.send(request.get("name"));

        Map<String, String> ret = new HashMap<>();
        ret.put("message", "sending message...");
        return ret;
    }

}
