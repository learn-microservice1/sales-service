package com.example.fila.demo.repository;

import com.example.fila.demo.entity.SalesDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalesDetailRepo extends JpaRepository<SalesDetail, Long> {
}
