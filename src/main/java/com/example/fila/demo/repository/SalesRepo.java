package com.example.fila.demo.repository;

import com.example.fila.demo.entity.Sales;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalesRepo extends JpaRepository<Sales, Integer> {

//    @Query(value = "SELECT * FROM t_sales WHERE invoiceNumber = ?1", nativeQuery = true)
    Sales getSalesByInvoiceNumber(String invoiceNumber);

}
