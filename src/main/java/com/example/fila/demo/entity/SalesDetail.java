package com.example.fila.demo.entity;

import javax.persistence.*;

@Entity
@Table(name = "t_sales_detail")
public class SalesDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer tSalesId;
    private Integer mStuffId;
    private Integer totalStuff;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer gettSalesId() {
        return tSalesId;
    }

    public void settSalesId(Integer tSalesId) {
        this.tSalesId = tSalesId;
    }

    public Integer getmStuffId() {
        return mStuffId;
    }

    public void setmStuffId(Integer mStuffId) {
        this.mStuffId = mStuffId;
    }

    public Integer getTotalStuff() {
        return totalStuff;
    }

    public void setTotalStuff(Integer totalStuff) {
        this.totalStuff = totalStuff;
    }
}
